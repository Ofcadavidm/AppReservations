﻿using Xamarin.Forms;

namespace AppReservations.Cells
{
    public class FligthCell : ViewCell
    {
        public FligthCell()
        {
            var fligthNoLabel = new Label
            {
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.Bold,
            };

            fligthNoLabel.SetBinding(Label.TextProperty, new Binding("FligthNo"));

            var fligthDateLabel = new Label
            {
                HorizontalTextAlignment = TextAlignment.End,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
            };

            fligthDateLabel.SetBinding(Label.TextProperty, new Binding("FligthDate", stringFormat: "{0:d}"));

            var departureLabel = new Label
            {
                HorizontalOptions = LayoutOptions.StartAndExpand,
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.None,
            };

            departureLabel.SetBinding(Label.TextProperty, new Binding("Departure"));

            var arrivalLabel = new Label
            {
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.None,
            };

            arrivalLabel.SetBinding(Label.TextProperty, new Binding("Arrival"));

            View = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children = { fligthNoLabel, fligthDateLabel, departureLabel, arrivalLabel, },
            };
        }
    }
}
