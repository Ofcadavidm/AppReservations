﻿using Xamarin.Forms;

namespace AppReservations.Cells
{
    public class ReservationCell: ViewCell
    {
        public ReservationCell()
        {
            var ReservationIdLabel = new Label
            {
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.Bold,
            };

            ReservationIdLabel.SetBinding(Label.TextProperty, new Binding("ReservationId"));

            var ReservationFligthLabel = new Label
            {
                HorizontalTextAlignment = TextAlignment.End,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
            };

            ReservationFligthLabel.SetBinding(Label.TextProperty, new Binding("ReservationFligth"));

            var PassangerdniLabel = new Label
            {
                HorizontalOptions = LayoutOptions.StartAndExpand,
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.None,
            };

            PassangerdniLabel.SetBinding(Label.TextProperty, new Binding("PassengerDni"));

            var PassengerNameLabel = new Label
            {
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.None,
            };

            PassengerNameLabel.SetBinding(Label.TextProperty, new Binding("PassengerName"));

            View = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Children = { ReservationIdLabel, ReservationFligthLabel, PassangerdniLabel, PassengerNameLabel, },
            };
        }
    }
}
 