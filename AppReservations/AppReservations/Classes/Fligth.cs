﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace AppReservations.Classes
{
    public class Fligth
    {
        [PrimaryKey, AutoIncrement]
        public int FligthId { get; set; }

        [Unique]
        public string FligthNo { get; set; }

        public decimal FligthPrice { get; set; }

        public DateTime FligthDate { get; set; }

        public string Departure { get; set; }

        public string DepartureTime { get; set; }

        public string Arrival { get; set; }

        public string ArrivalTime { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Reservation> Reservations { get; set; }

        public override int GetHashCode()
        {
            return FligthId;
        }
        /* para que los ListView se puedan visualisar de forma correcta */
        public override string ToString()
        {
            return string.Format("{0} {1:d} {2} {3}", FligthNo, FligthDate, Departure, Arrival);
        }
    }
}
