﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;

namespace AppReservations.Classes
{
    public class Reservation
    {
        [PrimaryKey, AutoIncrement]
        public int ReservationId { get; set; }

        public DateTime DateRegistered { get; set; }

        public int FligthId { get; set; }

        [ManyToOne]
        public Fligth Fligth { get; set; }

        public string ReservationFligth { get; set; }

        public string PassengerDni { get; set; }

        public string PassengerName { get; set; }

        public override int GetHashCode()
        {
            return ReservationId;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", ReservationId, ReservationFligth, PassengerDni, PassengerName);
        }
    }
}
