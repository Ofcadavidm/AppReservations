﻿using SQLite.Net.Interop;

namespace AppReservations.Interfaces
{
    public interface IConfig
    {
        string DirectoryDB { get; }

        ISQLitePlatform Platform { get; }
    }
}
