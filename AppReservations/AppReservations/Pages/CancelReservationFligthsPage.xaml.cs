﻿using AppReservations.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppReservations.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CancelReservationFligthsPage : ContentPage
    {
        private Reservation reservation;

        public CancelReservationFligthsPage(Reservation reservation)
        {
            InitializeComponent();

            this.reservation = reservation;

            ReservationIdEntry.Text = String.Format("{0}", reservation.ReservationId);
            DateRegisteredDatePicker.Date = reservation.DateRegistered;
            ReservationFligthEntry.Text = reservation.ReservationFligth;
            passengerDniEntry.Text = reservation.PassengerDni;
            passengerNameEntry.Text = reservation.PassengerName;

            deleteButton.Clicked += DeleteButton_Clicked;
        }
        private async void DeleteButton_Clicked(object sender, EventArgs e)
        {

            var respuesta = await DisplayAlert("Confirmación", "Esta seguro de cancelar ?", "Si", "No");
            if (!respuesta)
            {
                return;
            }

            using (var da = new DataAccess())
            {
                da.Delete(reservation);
            }

            await DisplayAlert("Mensaje", "Reserva Cancelada", "Aceptar");
            await Navigation.PopAsync();
        }
    }
}