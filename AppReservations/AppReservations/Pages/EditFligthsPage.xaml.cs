﻿using AppReservations.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppReservations.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditFligthsPage : ContentPage
    {
        private Fligth fligth;

        public EditFligthsPage(Fligth fligth)
        {
            InitializeComponent();

            this.fligth = fligth;


            fligthNoEntry.Text = fligth.FligthNo;
            fligthdateDatePicker.Date = fligth.FligthDate;
            departureEntry.Text = fligth.Departure;
            departuretimeEntry.Text = fligth.DepartureTime;
            arrivalEntry.Text = fligth.Arrival;
            arrivaltimeEntry.Text = fligth.ArrivalTime;

            updateButton.Clicked += UpdateButton_Clicked;
            deleteButton.Clicked += DeleteButton_Clicked;
        }

        private async void DeleteButton_Clicked(object sender, EventArgs e)
        {

            var respuesta = await DisplayAlert("Confirmación", "Esta seguro de eliminar ?", "Si", "No");
            if (!respuesta)
            {
                return;
            }

            using (var da = new DataAccess())
            {
                var reservation = da.GetList<Reservation>(false).Where(r => r.FligthId == fligth.FligthId).FirstOrDefault();
                if (reservation != null)
                {
                    await DisplayAlert("Mensaje", "El registro no puede ser eliminado, tiene reservas asociadas", "Aceptar");
                    return;
                }

                da.Delete(fligth);
            }

            await DisplayAlert("Mensaje", "Rgistro Eliminado", "Aceptar");
            await Navigation.PopAsync();
        }

        private async void UpdateButton_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(fligthNoEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar Nro.Vuelo", "Acept");
                return;
            }

            if (string.IsNullOrEmpty(departureEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar ciudad de salida", "Aceptar");
                return;
            }
            if (string.IsNullOrEmpty(departuretimeEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar hora(hh:mi) de salida", "Aceptar");
                return;
            }

            if (string.IsNullOrEmpty(arrivalEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar ciudad de llegada", "Aceptar");
                return;
            }
            if (string.IsNullOrEmpty(arrivaltimeEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar hora de llegada", "Aceptar");
                return;
            }


            fligth.FligthNo = fligthNoEntry.Text;
            fligth.FligthDate = fligthdateDatePicker.Date;
		    fligth.Departure = departureEntry.Text;
		    fligth.DepartureTime = departuretimeEntry.Text;
		    fligth.Arrival = arrivalEntry.Text;
		    fligth.ArrivalTime = arrivaltimeEntry.Text;
			
        using (var da = new DataAccess())
            {
                da.Update(fligth);
            }

            await DisplayAlert("Mensaje", "Registro actualizado", "Aceptar");
            await Navigation.PopAsync();
        }
    }
}