﻿using AppReservations.Cells;
using AppReservations.Classes;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppReservations.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FligthQuerysPage : ContentPage
    {
        public FligthQuerysPage()
        {
            InitializeComponent();
            fligthsListView.ItemTemplate = new DataTemplate(typeof(FligthCell));
            fligthsListView.ItemSelected += fligthsListView_ItemSelected;
            searchButton.Clicked += SearchButton_Clicked;
        }
      
         private async void fligthsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            await Navigation.PushAsync(new ReservationFligthsPage((Fligth)e.SelectedItem));
        }
        
        /* sobre escribir metodo OnAppearing para refrescar la lista */
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            using (var da = new DataAccess())
            {
                fligthsListView.ItemsSource = da.GetList<Fligth>(false).Where(f => f.FligthNo =="****").OrderBy(f => f.FligthNo);
            }
        }
       
        private async void SearchButton_Clicked(object sender, EventArgs e)
        {
           
            using (var da = new DataAccess())
            {

                var list = da.GetList<Fligth>(false).
                    Where(
                            f => (f.FligthDate.Year == fligthdateDatePicker.Date.Year &&
                                  f.FligthDate.Month == fligthdateDatePicker.Date.Month &&
                                  f.FligthDate.Day == fligthdateDatePicker.Date.Day) ||
                                  f.FligthNo == fligthNoEntry.Text ||
                                  f.Departure == departureEntry.Text || 
                                  f.Arrival == arrivalEntry.Text
                          ).OrderBy(f => f.FligthNo)
                          .ToList();
                if (list.Count==0)
                {
                    await DisplayAlert("Consulta", "No existen registros para los criterios indicados", "Aceptar");
                    fligthsListView.ItemsSource = string.Empty; 
                    return;
                }
                fligthsListView.ItemsSource = list;
            }

           
            fligthNoEntry.Text = string.Empty;
            fligthdateDatePicker.Date = DateTime.Now;
            departureEntry.Text = string.Empty;           
            arrivalEntry.Text = string.Empty;
        }
    }
}