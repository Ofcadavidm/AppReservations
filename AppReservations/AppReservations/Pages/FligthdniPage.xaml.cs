﻿using AppReservations.Cells;
using AppReservations.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppReservations.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FligthdniPage : ContentPage
    {
        public FligthdniPage()
        {
            InitializeComponent();
            reservationsListView.ItemTemplate = new DataTemplate(typeof(ReservationCell));
            searchButton.Clicked += SearchButton_Clicked;
        }
        private async void SearchButton_Clicked(object sender, EventArgs e)
        {

            using (var da = new DataAccess())
            {
                
                var list = da.GetList<Reservation>(false).
                    Where(r => r.PassengerDni == passengerDniEntry.Text).
                    OrderBy(r => r.DateRegistered).
                    ToList();
                if (list.Count == 0)
                {
                    await DisplayAlert("Consulta", "No existen reservaciones para la cédula indicada", "Aceptar");
                    reservationsListView.ItemsSource = string.Empty;
                    passengerDniEntry.Text = string.Empty;
                    return;
                }
                reservationsListView.ItemsSource = list;
            }

            passengerDniEntry.Text = string.Empty;
            
        }
    }
}