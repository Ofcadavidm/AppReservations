﻿using AppReservations.Cells;
using AppReservations.Classes;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppReservations.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FligthsPage : ContentPage
    {
        public FligthsPage()
        {
            InitializeComponent();


            fligthsListView.ItemTemplate = new DataTemplate(typeof(FligthCell));
            fligthsListView.ItemSelected += fligthsListView_ItemSelected;
            addButton.Clicked += AddButton_Clicked;
        }

        private async void fligthsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            await Navigation.PushAsync(new EditFligthsPage((Fligth)e.SelectedItem));
        }

        /* sobre escribir metodo OnAppearing para refrescar la lista */
        protected override void OnAppearing()
        {
            base.OnAppearing();
            using (var da = new DataAccess())
            {
                fligthsListView.ItemsSource = da.GetList<Fligth>(false).OrderBy(f => f.FligthNo);
            }
        }

        private async void AddButton_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(fligthNoEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar el nro. del vuelo", "Aceptar");
                return;
            }

            if (string.IsNullOrEmpty(departureEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar ciudad de salida", "Aceptar");
                return;
            }
            if (string.IsNullOrEmpty(departuretimeEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar hora( hh:mi) de salida", "Aceptar");
                return;
            }

            var regex = new System.Text.RegularExpressions.Regex(@"^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$");

            if (!regex.IsMatch(departuretimeEntry.Text))
            {
                await DisplayAlert("Error", "hora de salida debe ser numerico", "Aceptar");
                return;
            }

            if (string.IsNullOrEmpty(arrivalEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar ciudad de llegada", "Aceptar");
                return;
            }
            if (string.IsNullOrEmpty(arrivaltimeEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar hora de llegada", "Aceptar");
                return;
            }
            if (!regex.IsMatch(arrivaltimeEntry.Text))
            {
                await DisplayAlert("Error", "hora de llegada debe ser numerico", "Aceptar");
                return;
            }

            var fligth = new Fligth
            {

                FligthNo = fligthNoEntry.Text,
                FligthDate = fligthdateDatePicker.Date,
                Departure = departureEntry.Text,
                DepartureTime = departuretimeEntry.Text,
                Arrival = arrivalEntry.Text,
                ArrivalTime = arrivaltimeEntry.Text,
            };

            using (var da = new DataAccess())
            {
                da.Insert(fligth);
                await DisplayAlert("Message", "Vuelo creado de forma correcta", "Aceptar");
                fligthsListView.ItemsSource = da.GetList<Fligth>(false).OrderBy(f => f.FligthNo);
            }


            fligthNoEntry.Text = string.Empty;
            fligthdateDatePicker.Date = DateTime.Now;
            departureEntry.Text = string.Empty;
            departuretimeEntry.Text = string.Empty;
            arrivalEntry.Text = string.Empty;
            arrivaltimeEntry.Text = string.Empty;
        }
    }
}