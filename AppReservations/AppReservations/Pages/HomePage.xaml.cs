﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppReservations.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();

            fligthsButton.Clicked += fligthsButton_Clicked;
            fligthQueryButton.Clicked += fligthQueryButton_Clicked;
            fligthdniButton.Clicked += fligthdniButton_Clicked;
        }

        private async void fligthsButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new FligthsPage());
        }

        private async void fligthQueryButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new FligthQuerysPage());
        }

        private async void fligthdniButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new FligthdniPage());
        }
    }
    
}