﻿using AppReservations.Cells;
using AppReservations.Classes;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace AppReservations.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReservationFligthsPage : ContentPage
    {
        private Fligth fligth;

        public ReservationFligthsPage(Fligth fligth)
        {
            InitializeComponent();
            reservationsListView.ItemTemplate = new DataTemplate(typeof(ReservationCell));
            reservationsListView.ItemSelected += reservationsListView_ItemSelected;

            this.fligth = fligth;

            var fligthId = fligth.FligthId;
            fligthNoEntry.Text = fligth.FligthNo;
            fligthdateDatePicker.Date = fligth.FligthDate;
            departureEntry.Text = fligth.Departure;
            arrivalEntry.Text = fligth.Arrival;

            reservationButton.Clicked += ReservationButton_Clicked;
        }

        private async void reservationsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            await Navigation.PushAsync(new CancelReservationFligthsPage((Reservation)e.SelectedItem));
        }

        /* sobre escribir metodo OnAppearing para refrescar la lista */

        protected override void OnAppearing()
        {
            base.OnAppearing();
            using (var da = new DataAccess())
            {
                reservationsListView.ItemsSource = da.GetList<Reservation>(false).OrderBy(r => r.ReservationId);
            }
        }



        private async void ReservationButton_Clicked(object sender, EventArgs e)
        {

                    if (string.IsNullOrEmpty(passengerDniEntry.Text))
                    {
                        await DisplayAlert("Error", "Debe ingresar Nro. Cédula", "Aceptar");
                        return;
                    }

                    if (string.IsNullOrEmpty(passengerNameEntry.Text))
                    {
                        await DisplayAlert("Error", "Debe ingresar Nombre", "Aceptar");
                        return;
                    }

                    var reservation = new Reservation
                    {

                        DateRegistered = DateTime.Now,
                        FligthId = fligth.FligthId,
                        ReservationFligth = fligthNoEntry.Text,
                        PassengerDni = passengerDniEntry.Text,
                        PassengerName= passengerNameEntry.Text,

                    };

                    using (var da = new DataAccess())
                    {
                        da.Insert(reservation);
                        await DisplayAlert("Mensaje", "Vuelo Reservado de forma correcta", "Aceptar");
                        reservationsListView.ItemsSource = da.GetList<Reservation>(false).OrderBy(r => r.ReservationId);
                    }


                    fligthNoEntry.Text = string.Empty;
                    fligthdateDatePicker.Date = DateTime.Now;
                    departureEntry.Text = string.Empty;
                    arrivalEntry.Text = string.Empty;
                    passengerDniEntry.Text = String.Empty;
                    passengerNameEntry.Text = String.Empty;

                   
        }
    }
}